import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticatedGuard, RolGuard } from '@dipos-ris-web-commons';
import { UserRol } from '@dipos-ris-web/models';
import { NotFoundComponent } from './core/components/not-found/not-found.component';
import { InicioComponent } from './features/website/views/inicio/inicio.component';

const routes: Routes = [
  {
    path: 'admin',
    //canActivate: [AuthenticatedGuard], //, RolGuard
    // data: { rols: [UserRol.admin] },
    loadChildren: () => import('./features/admin/admin.module').then(m => m.AdminModule)
  },
  {
    path: '', 
    loadChildren: () => import('./features/website/website.module').then(m => m.WebsiteModule)
  },
  {
    path: 'website',
    loadChildren: () => import('./features/website/website.module').then(m => m.WebsiteModule)

  },
  {
    path: 'auth',
    loadChildren: () => import('./features/auth/auth.module').then(m => m.AuthModule)
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top', useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
