import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { SessionService } from '@dipos-ris-web-commons';
import { filter } from 'rxjs/operators';
import { ISidebarMenu } from '../../../features/admin/interfaces/menu.interface';
import { options } from './sidebar.constants';
import { UserRol } from '../../../../../../../libs/dipos-ris-web-commons/src/lib/models/user.model';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() toggle: boolean = false;

  @Output() toggleChange = new EventEmitter<boolean>();

  options: ISidebarMenu[] = options;
  user: any;
  rolAccess: any;

  constructor(
    private router: Router,
    private session: SessionService
  ) { 
    this.user = this.session.user;
    this.rolAccess = this.session.user?JSON.stringify(this.session.user.userRol):[];
    
  }

  ngOnInit() {    
    this.selectionItem();

    // Nos suscribimos al evento de fin de navegación para identificar el menu activo
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd)
      )
      .subscribe(event => {        
        this.clearSelection();
        this.selectionItem();
      });
  }

  selectionItem(): void {
    this.options.forEach(item => {
      if (!item.childrens) {
        item.selected = this.isActiveRoute(item.url);
      }

      item.childrens?.forEach(element => {

        if (!element.childrens) {
          element.selected = this.isActiveRoute(element.url);
        }

        element.childrens?.forEach(childelement =>{
          const sel = this.isActiveRoute(childelement.url);
          // console.log(element.url, sel);
          if (sel) {
            item.selected = sel;
            item.expand = sel;
            element.selected = sel;
          }
        })

  

      });
    });
  }

  isActiveRoute(url: string) {
    return this.router.isActive(url, {
      paths: 'subset',
      queryParams: 'subset',
      fragment: 'ignored',
      matrixParams: 'ignored'
    });
  }

  toggleMenu(item: ISidebarMenu): void {
    //let conta = 0;
    this.options.forEach(x => {
      if (x != item) x.expand = false;
      //if (x != item) x.childrens = this.options[conta];
    });
    item.expand = !item.expand;
    console.log("expand menu", item.expand)
  }
  
  toggleSubMenu(item: ISidebarMenu): void {

    this.options.forEach(x => {

      if(x.childrens){
        x.childrens.forEach(y  =>{

          if(y.childrens){
            if (y != item) y.expand = false;
          }
        })
      }
    });
    item.expand = !item.expand;
    console.log("expand menu",item, item.expand)
  }

  

  toggleSubSubMenu(item: ISidebarMenu): void {

    this.options.forEach(x => {

      if(x.childrens){
        x.childrens.forEach(y  =>{

          if(y.childrens){

            y.childrens.forEach(z => {
              if (z != item) z.expand = false;
            });

           
          }
        })
      }
    });
    item.expand = !item.expand;
    console.log("expand menu",item, item.expand)
  }


  clearSelection(): void {
    this.options.forEach(item => {
      item.selected = false;
      item.expand = false;
      item.childrens?.forEach(el => el.selected = false);
    });
  }

  signOut(): void {
    this.session.destroy();
    this.user = [];
    this.router.navigateByUrl('/website');
  }

  signIn() : void {
    this.router.navigateByUrl('/auth');
  }

  redirect(url: string){
    this.router.navigateByUrl(url);
  }

  toggleActive(): void {
    this.toggle = !this.toggle;
    localStorage.setItem('sidebar', this.toggle ? 'open' : 'close');
    this.toggleChange.emit(this.toggle);

    // Trigger for resize apx-graphs
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 250);
  }
  
}
