import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeorismapComponent } from './georismap.component';

describe('GeorismapComponent', () => {
  let component: GeorismapComponent;
  let fixture: ComponentFixture<GeorismapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeorismapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeorismapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
