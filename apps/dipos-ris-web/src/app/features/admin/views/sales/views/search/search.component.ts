import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OrderService } from '@dipos-ris-web-commons';
import { map } from 'rxjs/operators';
import { CustomerListComponent } from '../../../customers/components/customer-list/customer-list.component';

@Component({
  selector: 'app-sales-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SalesSearchComponent implements OnInit {
  formSearch: FormGroup;
  form: FormGroup;
  paginationController: any = {};
  orders: any[] = [];
  isLoading: boolean;

  constructor(
    private orderService: OrderService,
    private fb: FormBuilder,
    private bsModal: BsModalService
  ) {
    this.builder();
    this.quickSearch();
  }

  ngOnInit() {
    this.formSearch.get('quick').setValue(2);
    // this.search();
  }

  builder(): void {
    this.formSearch = this.fb.group({
      option: 1,
      quick: null,
    });

    this.form = this.fb.group({
      id_order: null,
      client: null,
      id_state: null,
      id_employee: null,
      employee: null,
      date_start: null,
      date_end: null
    });
  }

  search(): void {
    this.removeValidators();

    const values = this.form.value;

    if (!values.id_order) {
      this.form.get('date_start').setValidators(Validators.required);
      this.form.get('date_end').setValidators(Validators.required);
      this.form.get('date_start').updateValueAndValidity();
      this.form.get('date_end').updateValueAndValidity();
    }

    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    this.isLoading = true;
    this.orderService.search(values)
      .pipe(
        map(res => {
          return res.map(item => {
            item.overview = false;
            item.loading = false;
            item.details = null;
            return item;
          })
        })
      ).subscribe(res => {
        this.orders = res;
        this.isLoading = false;
      }, () => this.isLoading = false);
  }

  dateTo(days: number): Date {
    return new Date(new Date().getTime() - days * 24 * 60 * 60 * 1000);
  }

  quickSearch(): void {
    this.formSearch.get('quick').valueChanges.subscribe(value => {
      let start = new Date();
      let end = new Date();

      switch (+value) {
        case 2:
        case 7:
        case 15:
        case 30:
          start = this.dateTo(+value);
          break;
        case 365:
          start = new Date(end.getFullYear(), 0, 1);
          break;
      }

      this.form.patchValue({
        date_start: start,
        date_end: end
      });

      this.search();
    });
  }

  removeValidators() {
    for (const key in this.form.controls) {
      this.form.get(key).clearValidators();
      this.form.get(key).updateValueAndValidity();
    }
  }

  reset(): void {
    this.removeValidators();
    this.form.reset();
  }

  paginationChange(e: any): void {
    this.paginationController = e;
  }

  overview(item: any): void {
    item.overview = !item.overview;

    if (item.overview) {
      item.loading = true;
      this.orderService.getById(item.IdPedido).subscribe(res => {
        item.details = res;
        item.loading = false;
      }, () => item.loading = false);
    }
  }

  clientSearch(): void {
    this.bsModal.open(CustomerListComponent).then(res => {
      if (res) {
        this.form.get('client').setValue(res.RazonSocial);
      }
    });
  }

}
