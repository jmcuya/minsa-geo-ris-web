import { ChartOptions } from '@admin-shared/types/chart-type';
import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { OrderService, SessionService, StatisticsService } from '@dipos-ris-web-commons';
import { UserModel } from '@dipos-ris-web/models';
import { ChartComponent } from "ng-apexcharts";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  user: UserModel;
  products = [];
  resume = { Cash: 0, Credit: 0, Percent: 0 };
  sales = [];
  salesController: any = {};

  isLoadingGraph: boolean;
  isLoadingResume: boolean;
  isLoadingSales: boolean;

  constructor(
    private session: SessionService,
    private statisticsService: StatisticsService,
    private orderService: OrderService
  ) {

  }

  ngOnInit() {
    this.user = this.session.user;

    this.getSales();
    this.getResume();
    this.generateGraph();
  }

  generateGraph(): void {
    this.chartOptions = {
      series: [],
      chart: {
        id: "chart",
        type: "line",
        width: "100%",
        height: 350,
        toolbar: {
          autoSelected: "pan",
          // show: false
        }
      },
      // colors: ["#4723d9", "#adb5bd"],
      stroke: {
        width: 3,
        curve: 'smooth'
      },
      dataLabels: {
        enabled: false
      },
      fill: {
        opacity: 1
      },
      markers: {
        size: 5
      },
      xaxis: {
        type: "datetime"
      }
    };

    this.isLoadingGraph = true;

    const end = new Date();
    const start = new Date(end.getTime() - 30 * 24 * 60 * 60 * 1000);

    this.statisticsService.sales(start.toISOString(), end.toISOString()).subscribe(res => {
      this.isLoadingGraph = false;

      const series = [];
      const ventas = {
        name: "Ventas",
        color: '#4723d9',
        data: res.map(value => ([value.Fecha, value.Ventas])) as any[]
      };

      series.push(ventas);

      if (this.user.isAdmin()) {
        const utilidades = {
          name: "Utilidades",
          color: '#ffc107',
          data: res.map(value => ([value.Fecha, value.Utilidad])) as any[]
        };

        series.push(utilidades);
      }

      // Para agregar las series y actualizar la animacion
      this.chart.updateSeries(series);
    }, () => this.isLoadingGraph = false);
  }

  getResume(): void {
    this.isLoadingResume = true;
    this.statisticsService.resume().subscribe(res => {
      this.isLoadingResume = false;
      this.products = res.Products;

      // Resumen
      this.resume.Cash = res.DailyResume?.Contado || 0;
      this.resume.Credit = res.DailyResume?.Credito || 0;

      const total = this.resume.Cash + this.resume.Credit;
      if (total) {
        this.resume.Percent = Math.round(this.resume.Cash * 100 / total);
      }

    }, () => this.isLoadingResume = false);
  }

  getSales(): void {
    this.isLoadingSales = true;
    this.orderService.search({ Top: 30 }).subscribe(res => {
      this.isLoadingSales = false;
      this.sales = res;
    }, () => this.isLoadingSales = false);
  }

  paginationChange(event): void {
    this.salesController = event;
  }

  errorImage(event: any): void {
    event.onerror = null;
    event.target.src = './assets/no-photos.png';
  }

}
