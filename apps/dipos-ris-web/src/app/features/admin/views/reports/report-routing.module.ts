import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ReportsComponent } from "./reports.component";
import { CustomersReportComponent } from "./views/customers-report/customers-report.component";
import { IncomesExpensesReportComponent } from "./views/incomes-expenses-report/incomes-expenses-report.component";
import { InventoryReportComponent } from "./views/inventory-report/inventory-report.component";
import { PaymentReportComponent } from "./views/payment-report/payment-report.component";
import { ProductsReportComponent } from "./views/products-report/products-report.component";
import { ReceiveReportComponent } from "./views/receive-report/receive-report.component";
import { SalesReportComponent } from "./views/sales-report/sales-report.component";
import { UtilitiesReportComponent } from "./views/utilities-report/utilities-report.component";

const routes: Routes = [
  {
    path: '',
    component: ReportsComponent,
    children: [
      { path: '', redirectTo: 'ventas', pathMatch: 'full' },
      {
        path: 'ventas',
        component: SalesReportComponent
      },
      {
        path: 'ventas-por-producto',
        component: ProductsReportComponent
      },
      {
        path: 'ventas-por-cliente',
        component: CustomersReportComponent
      },
      {
        path: 'cuentas-por-cobrar',
        component: ReceiveReportComponent
      },
      {
        path: 'cuentas-por-pagar',
        component: PaymentReportComponent
      },
      {
        path: 'ingresos-gastos',
        component: IncomesExpensesReportComponent
      },
      {
        path: 'utilidades-por-venta',
        component: UtilitiesReportComponent
      },
      {
        path: 'inventario-valorizado',
        component: InventoryReportComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }