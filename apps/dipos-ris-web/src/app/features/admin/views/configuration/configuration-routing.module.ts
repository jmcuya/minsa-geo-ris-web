import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ConfigurationComponent } from "./configuration.component";
import { UsersComponent } from "./views/users/users.component";

const routes: Routes = [
    {
        path: '',
        component: ConfigurationComponent,
        children: [
            { path: '', pathMatch: 'full', redirectTo: 'usuarios' },
            {
                path: 'usuarios',
                component: UsersComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConfigurationRoutingModule { }
