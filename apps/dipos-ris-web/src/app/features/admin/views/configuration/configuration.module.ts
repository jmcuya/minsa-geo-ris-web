import { NgModule } from '@angular/core';
import { ConfigurationComponent } from './configuration.component';
import { ConfigurationRoutingModule } from './configuration-routing.module';
import { AdminSharedModule } from '@admin-shared/shared.module';
import { UsersComponent } from './views/users/users.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { ProfileComponent } from './components/profile/profile.component';
// ngx-bootstrap
// import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

@NgModule({
  imports: [
    ConfigurationRoutingModule,
    AdminSharedModule,
    // BsDatepickerModule.forRoot(),
  ],
  declarations: [
    ConfigurationComponent,
    UsersComponent,
    ProfileComponent,
    UserEditComponent
  ]
})
export class ConfigurationModule { }
