import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TumbesRisComponent } from './tumbes-ris.component';

describe('TumbesRisComponent', () => {
  let component: TumbesRisComponent;
  let fixture: ComponentFixture<TumbesRisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TumbesRisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TumbesRisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
