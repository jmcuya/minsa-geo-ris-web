import { AdminSharedModule } from "@admin-shared/shared.module";
import { NgModule } from "@angular/core";
import { CustomerCreateComponent } from "./customer-create/customer-create.component";
import { CustomerListComponent } from "./customer-list/customer-list.component";

const COMPONENTS = [
  CustomerListComponent,
  CustomerCreateComponent
];

@NgModule({
  imports: [
    AdminSharedModule
  ],  
  declarations: [...COMPONENTS],
  exports: [...COMPONENTS],
})
export class CustomerComponentsModule { }
