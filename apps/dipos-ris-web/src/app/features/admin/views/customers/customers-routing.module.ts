import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CustomersComponent } from "./customers.component";
import { CustomerRegisterComponent } from "./views/register/register.component";
import { CustomerSearchComponent } from "./views/search/search.component";

const routes: Routes = [
  {
    path: '',
    component: CustomersComponent,
    children: [
      {
        path: '',
        component: CustomerSearchComponent
      },
      {
        path: 'registro',
        component: CustomerRegisterComponent
      },
      {
        path: 'registro/:id',
        component: CustomerRegisterComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule { }