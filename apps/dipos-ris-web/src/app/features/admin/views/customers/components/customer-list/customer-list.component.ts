import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { OwnerService } from '@dipos-ris-web-commons';
declare var bootstrap: any;

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {

  formSearch: FormGroup;

  data: any[] = [];
  paginationController: any = {};
  ownerId: number;

  constructor(
    private fb: FormBuilder,
    private ownerService: OwnerService,
    private bsModal: BsModalService
  ) {
    this.builder();
  }

  ngOnInit() {
    this.search();
  }

  builder(): void {
    this.formSearch = this.fb.group({
      type: null,
      param_type: null,
      param_text: null
    });
  }

  search(): void {
    const params = this.formSearch.value;
    this.ownerService.getAll(params).subscribe(res => {
      this.data = res;
    });
  }

  selected(item: any): void {
    this.bsModal.close(item);
  }

  edit(item: any): void {
    this.ownerId = item.IdPropietario;
    const triggerTab = document.querySelector('[data-bs-target="#tab-2"]') as any;
    triggerTab.click();
  }

  onSaveOwner(event): void {
    const triggerTab = document.querySelector('[data-bs-target="#tab-1"]') as any;
    triggerTab.click();

    this.search();
  }

}
