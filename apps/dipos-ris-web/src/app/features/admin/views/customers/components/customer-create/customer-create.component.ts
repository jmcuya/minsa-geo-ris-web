import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OwnerService } from '@dipos-ris-web-commons';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-customer-create',
  templateUrl: './customer-create.component.html',
  styleUrls: ['./customer-create.component.scss']
})
export class CustomerCreateComponent implements OnInit {

  @Input() ownerId: number;
  @Output() onSave = new EventEmitter();
  form: FormGroup;
  isLoading: boolean;

  constructor(
    private fb: FormBuilder,
    private ownerService: OwnerService,
    private toastr: ToastrService
  ) {
    this.build();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.ownerId) {
      this.isLoading = true;
      this.ownerService.byId(this.ownerId).subscribe(res => {
        this.form.patchValue(res);
        this.isLoading = false;
      }, () => this.isLoading = false);
    }
  }

  ngOnInit(): void {
  }

  build(): void {
    this.form = this.fb.group({
      IdPropietario: null,
      IdTipoDocumento: null,
      NroDocumento: null,
      RazonSocial: [null, Validators.required],
      Direccion: null,
      Email: [null, Validators.email],
      Celular: null,
      Observaciones: null,
      FechaRegistro: null,
      IdPersonalRegistro: null,
      FlagCliente: true,
      FlagProveedor: false,
      FlagActivo: true
    });
  }

  save(): void {
    if (this.form.valid) {
      const values = this.form.getRawValue();
      values.RazonSocial = values.RazonSocial.toUpperCase();
      values.Email = values.Email?.toLowerCase();

      this.isLoading = true;

      this.ownerService.register(values).subscribe(res => {
        this.form.get('IdPropietario').setValue(res);
        this.onSave.emit(this.form.getRawValue());
        this.isLoading = false;
        this.toastr.success('Los datos se han registrado exitosamente');
      }, () => this.isLoading = false);
    }
  }

  reset(): void {
    this.form.reset({ FlagCliente: true, FlagActivo: true });
  }

  getData(): void {
    const document = this.form.get('NroDocumento').value;
    this.isLoading = true;
    this.ownerService.getDataByDocument(document).subscribe(res => {
      this.isLoading = false;
      if (res) {
        this.form.patchValue({
          RazonSocial: res.nombre,
          Direccion: res.domicilio,
        });
      }
    }, () => this.isLoading = false);
  }

}
