import { ChartOptions } from "@admin-shared/types/chart-type";

export const ShoppingOptions: Partial<ChartOptions> = {
  chart: {
    id: 'sparkline2',
    // group: 'sparklines',
    type: 'area',
    height: 150,
    sparkline: {
      enabled: true
    },
  },
  stroke: {
    curve: 'straight'
  },
  fill: {
    opacity: 1,
  },
  series: [],
  // labels: [...Array(24).keys()].map(n => `2018-09-0${n + 1}`),
  yaxis: {
    min: 0
  },
  xaxis: {
    type: 'datetime',
  },
  colors: ['#DCE6EC'],
  title: {
    text: 'S/ 0.00',
    offsetX: 30,
    style: {
      fontSize: '24px',
      // cssClass: 'apexcharts-yaxis-title'
    }
  },
  subtitle: {
    text: 'Compras',
    offsetX: 30,
    style: {
      fontSize: '14px',
      // cssClass: 'apexcharts-yaxis-title'
    }
  }
}