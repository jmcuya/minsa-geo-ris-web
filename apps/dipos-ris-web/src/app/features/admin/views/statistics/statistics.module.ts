import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { StatisticsComponent } from './statistics.component';
import { AdminSharedModule } from '@admin-shared/shared.module';
import { StatisticsSalesComponent } from './views/sales/sales.component';
import { StatisticsRoutingModule } from './statistics-routing.module';
import { NgApexchartsModule } from 'ng-apexcharts';

@NgModule({
  imports: [
    AdminSharedModule,
    StatisticsRoutingModule,
    NgApexchartsModule
  ],
  declarations: [
    StatisticsComponent,
    StatisticsSalesComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StatisticsModule { }
