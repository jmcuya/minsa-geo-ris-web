import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { ConfirmService } from '@admin-shared/services/confirm.service';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PaymentService } from '@dipos-ris-web-commons';
import { ToastrService } from 'ngx-toastr';
import { CustomerListComponent } from '../../../../../customers/components/customer-list/customer-list.component';
import { MovementsComponent } from '../../../../components/movements/movements.component';
import { OperationsComponent } from '../../../../components/operations/operations.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class PaymentRegisterComponent implements OnInit {

  form: FormGroup;
  isLoading: boolean;

  constructor(
    private activateRoute: ActivatedRoute,
    private location: Location,
    private router: Router,
    private bsModal: BsModalService,
    private fb: FormBuilder,
    private paymentService: PaymentService,
    private confirmService: ConfirmService,
    private toastr: ToastrService
  ) {
    this.build();
  }

  ngOnInit() {
    const id = +this.activateRoute.snapshot.paramMap.get('id');
    if (id) {
      this.getData(id);
    }
  }

  build(): void {
    this.form = this.fb.group({
      IdPago: [{ value: null, disabled: true }],
      TipoPago: 'PAGO REALIZADO',
      IdPropietario: [null, Validators.required],
      NombrePropietario: [{ value: null, disabled: true }],
      FlagUsaCaja: null,
      TipoDocumento: null,
      NroDocumento: null,
      Motivo: [null, Validators.required],
      FechaVencimiento: [null, Validators.required],
      IdPersonalPago: null,
      FechaPago: null,
      ImportePorPagar: [null, Validators.required],
      ImportePagado: null,
      IdMoneda: 'PEN',
      Observaciones: null,
      IdEstado: 1,
      IdPersonalRegistro: null,
      FechaRegistro: null,
      IdPersonalModificacion: null,
      FechaModificacion: null,
      IdPersonalAnulacion: null,
      FechaAnulacion: null,
    });
  }

  customerSearch(): void {
    this.bsModal.open(CustomerListComponent).then(res => {
      if (res) {
        this.form.patchValue({
          IdPropietario: res.IdPropietario,
          NombrePropietario: res.RazonSocial
        });
      }
    });
  }

  getData(id: number): void {
    this.isLoading = true;

    this.paymentService.get(id).subscribe(res => {
      this.isLoading = false;
      this.form.patchValue(res);
    }, () => this.isLoading = false);
  }

  newRegister(): void {
    this.form.reset({ TipoPago: 'PAGO REALIZADO', IdMoneda: 'PEN', IdEstado: 1 });
    this.router.navigateByUrl('/caja/pagos/registro');
  }

  save(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      this.form.updateValueAndValidity();
      return;
    }

    const values = this.form.getRawValue();
    if (!values.IdPago) {
      values.IdEstado = 1;
    }

    this.isLoading = true;
    this.paymentService.register(values).subscribe(res => {
      this.isLoading = false;

      this.form.get('IdPago').setValue(res);
      this.toastr.success('Se han guardado los datos exitosamente', 'Guardado');

      this.updateUrl();
    }, () => this.isLoading = false);
  }

  cancel(): void {
    this.confirmService.open('Anular', '¿Desea anular el registro actual?').then(success => {
      if (success) {
        const paymentId = this.form.get('IdPago').value;

        this.isLoading = true;
        this.paymentService.cancel(paymentId).subscribe(res => {
          this.isLoading = false;

          if (res) {
            this.form.get('IdEstado').setValue(3);
            this.toastr.success('Se ha anulado el registro actual', 'Anulado');
          }
        }, () => this.isLoading = false);
      }
    });
  }

  pay(): void {
    const confirmParams = {
      confirmText: 'Usar caja',
      cancelText: 'Registrar como pagado',
    };

    this.confirmService.open('Pagar importe', '¿Desea usar caja para pagar?', confirmParams).then(success => {
      if (typeof success !== 'boolean') return;

      if (success) {
        const params = {
          title: 'Registrar egreso',
          type: 'S',
          status: 2,
          concept_id: 8,
          amount: +this.form.get('ImportePorPagar').value,
          disabled_concepts: true,
          reference: 'PAGO REALIZADO',
          id_reference: this.form.get('IdPago').value
        }

        this.bsModal.open(OperationsComponent, { params }).then(res => {
          if (res) {
            this.registerPay(true);
          }
        });
      } else {
        this.registerPay();
      }
    });
  }

  registerPay(flgUseBox?: boolean): void {
    const values = this.form.getRawValue();
    values.FlagUsaCaja = !!flgUseBox;

    this.isLoading = true;
    this.paymentService.pay(values).subscribe(res => {
      this.isLoading = false;
      if (res) {
        this.form.get('IdEstado').setValue(2);
        this.toastr.success('Se ha registrado exitosamente el pago', 'Listo');
      } else {
        this.toastr.error('Hubo un problema. No se ha podido registrar el pago');
      }
    }, () => this.isLoading = false);
  }

  movements(): void {
    this.bsModal.open(MovementsComponent, { reference: 'PAGO REALIZADO', id_reference: this.form.get('IdPago').value });
  }

  updateUrl(): void {
    this.location.go(`/caja/pagos/registro/${this.form.get('IdPago').value}`);
  }

}
