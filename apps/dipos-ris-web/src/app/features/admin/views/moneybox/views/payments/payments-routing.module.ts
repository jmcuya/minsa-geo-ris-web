import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PaymentsComponent } from "./payments.component";
import { PaymentRegisterComponent } from "./views/register/register.component";
import { PaymentSearchComponent } from "./views/search/search.component";

const routes: Routes = [
  {
    path: '',
    component: PaymentsComponent,
    children: [
      {
        path: '',
        component: PaymentSearchComponent
      },
      {
        path: 'registro',
        component: PaymentRegisterComponent
      },
      {
        path: 'registro/:id',
        component: PaymentRegisterComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsRoutingModule { }
