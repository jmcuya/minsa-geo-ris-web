import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { ConfirmService } from '@admin-shared/services/confirm.service';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WarehouseService } from '@dipos-ris-web-commons';
import { BehaviorSubject } from 'rxjs';
import Swal from 'sweetalert2';
import { GuideItemComponent } from '../../components/guide-item/guide-item.component';

@Component({
  selector: 'app-internal-guide',
  templateUrl: './internal-guide.component.html',
  styleUrls: ['./internal-guide.component.scss']
})
export class InternalGuideComponent implements OnInit {
  warehouses = [];
  movements = [];
  movementsFilter = [];
  form: FormGroup;
  isLoading: boolean;

  constructor(
    private bsModal: BsModalService,
    private confirmService: ConfirmService,
    private warehouseService: WarehouseService,
    private fb: FormBuilder
  ) {
    this.build();
  }

  ngOnInit() {
    this.getWarehouses();
  }

  build(): void {
    this.form = this.fb.group({
      IdAlmacen: [null, Validators.required],
      TipoGuia: ['E', Validators.required],
      DocumentoSerie: null,
      DocumentoNro: null,
      FechaIngreso: [new Date(), Validators.required],
      IdMotivo: [null, Validators.required],
      IdMoneda: 'PEN',
      TasaCambio: null,
      FlagIncluyeIGV: true,
      DocumentoRef: null,
      DocumentoRefNro: null,
      GuiaDetalle: this.fb.array([], Validators.required)
    });

    this.changeProducts();
  }

  get products(): FormArray {
    return this.form.get('GuiaDetalle') as FormArray;
  }

  changeProducts(): void {
    this.products.valueChanges.subscribe(changes => {
      this.products.controls.forEach((control, index: number) => {
        const qty = control.get('Cantidad').value,
          price = control.get('Precio').value;

        const subtotal = qty * price;

        // if (control.get('FlagInafectoIGV').value) {
        //   inafecto += subtotal;
        // }

        // total += subtotal;

        control.get('Importe').setValue(subtotal, { emitEvent: false });
      });

    });
  }

  getWarehouses(): void {
    this.warehouseService.getAll().subscribe(res => {
      this.warehouses = res;
      this.form.get('IdAlmacen').setValue(res[0]?.IdAlmacen);
    });

    this.warehouseService.movementTypes().subscribe(res => {
      // Execepciones: VENTA, TRANSFERENCIA (SALIDA), TRANSFERENCIA (INGRESO)
      this.movements = res.filter(x => ![3, 4, 5].includes(x.IdMotivo));
      this.changeType();
    });
  }

  addItems(): void {
    const queue = new BehaviorSubject(null);
    queue.subscribe(res => {
      if (res) {
        const index = this.products.controls.findIndex(control => control.get('IdProducto').value == res.IdProducto);
        if (index === -1) {
          const row = this.fb.group({ ...res });
          this.products.push(row);
        } else {
          this.products.at(index).patchValue(res);
        }
      }
    });

    this.bsModal.open(GuideItemComponent, { queue });
  }

  changeType(): void {
    const guieType = this.form.get('TipoGuia').value;
    this.movementsFilter = this.movements.filter(x => x.Tipo == guieType);
  }

  save(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    const values = this.form.value;
    // console.log(values);

    this.isLoading = true;

    let movements = values.GuiaDetalle.map(item => {
      return {
        IdAlmacen: values.IdAlmacen,
        TipoMovimiento: values.TipoGuia,
        IdMotivo: values.IdMotivo,
        DocTipo: values.DocumentoRef,
        DocNro: values.DocumentoRefNro,
        Fecha: values.FechaIngreso,
        IdProducto: item.IdProducto,
        IdUnidadMedida: item.IdUnidadMedida,
        Cantidad: item.Cantidad,
        IdMoneda: values.IdMoneda,
        Costo: item.Costo,
        Precio: item.Precio
      }
    });

    this.warehouseService.movementInsert(movements).subscribe(res => {
      this.isLoading = false;
      if (res.Movimientos?.length) {
        this.form.reset({ FechaIngreso: new Date(), IdMoneda: 'PEN', FlagIncluyeIGV: true });
        this.products.clear();
        Swal.fire('Guardado', 'Se ha procesado la Guía Interna', 'success');
      } else {
        Swal.fire('Atención', 'No se han generado movimientos', 'warning');
      }
    }, () => {
      this.isLoading = false;
    });
  }

  clear(): void {
    this.confirmService.open('Nuevo', '¿Desea crear una nueva guía de ingreso / salida?').then(success => {
      if (success) {
        this.form.reset({ FechaIngreso: new Date(), IdMoneda: 'PEN', FlagIncluyeIGV: true });
      }
    });
  }

  removeProduct(index: number): void {
    this.products.removeAt(index);
  }

}
