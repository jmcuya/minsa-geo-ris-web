import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '@dipos-ris-web-commons';

@Component({
  selector: 'app-trademark',
  templateUrl: './trademark.component.html',
  styleUrls: ['./trademark.component.scss']
})
export class TrademarkComponent implements OnInit {

  form: FormGroup;
  @Input() data: any;

  constructor(
    private fb: FormBuilder,
    private modal: BsModalService,
    private productService: ProductService
  ) {

  }

  ngOnInit() {
    this.form = this.fb.group({
      IdMarca: null,
      FlagActivo: true,
      Detalle: [null, Validators.required]
    });

    if (this.data) {
      this.form.patchValue(this.data);
    }
  }

  save(): void {
    const trademark = this.form.value;
    trademark.Detalle = trademark.Detalle.toUpperCase();

    this.productService.trademarkInsert(trademark).subscribe(res => {
      trademark.IdMarca = res;
      this.modal.close(trademark);
    });
  }

}
