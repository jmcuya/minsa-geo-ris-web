import { NgModule } from '@angular/core';
import { AdminSharedModule } from '@admin-shared/shared.module';
import { InternalGuideComponent } from './views/internal-guide/internal-guide.component';
import { InventoryRoutingModule } from './guide-routing.module';
import { GuideItemComponent } from './components/guide-item/guide-item.component';
import { UnitProductComponent } from './components/unit-product/unit-product.component';

@NgModule({
  imports: [
    AdminSharedModule,
    InventoryRoutingModule
  ],
  declarations: [
    InternalGuideComponent,
    GuideItemComponent,
    UnitProductComponent
  ]
})
export class GuideModule { }
