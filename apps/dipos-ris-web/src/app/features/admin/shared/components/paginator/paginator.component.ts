import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit, OnChanges {

  @Input() total: number = 0;
  @Input() size: number = 10;
  @Input() pageSizes: number[] = [5, 10, 20, 50];
  @Output() pagination = new EventEmitter();

  current: number = 1;
  pages: number = 0;
  start: number = 0;
  end: number = 0;

  sizeControl = new FormControl();

  constructor() { }

  ngOnInit(): void {
    this.sizeControl.setValue(this.size);

    this.sizeControl.valueChanges.subscribe(value => {
      this.size = value;
      this.setPagination();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.total.currentValue !== changes.total.previousValue) {
      this.current = 1;
    }

    this.setPagination();
  }

  setPagination(): void {
    const pages = Math.ceil(this.total / this.size);
    const start = (this.current - 1) * this.size + 1;
    const current = this.current;
    let end = current * this.size;

    if (end > this.total) {
      end = this.total;
    }

    this.pages = pages;
    this.start = pages ? start : 0;
    this.end = end;    

    this.pagination.emit({ current, pages, start, end });
  }

  previousPage(): void {
    const current = this.current;
    if (current > 1) {
      this.goToPage(current - 1);
    }
  }

  nextPage(): void {
    const current = this.current;
    const pages = this.pages;
    if (current < pages) {
      this.goToPage(current + 1);
    }
  }

  goToPage(n: number): void {
    if (n > 0) {
      this.current = n;
      this.setPagination();
    }
  }

}
