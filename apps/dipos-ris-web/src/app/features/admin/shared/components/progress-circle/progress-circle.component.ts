import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from "@angular/core";

@Component({
  selector: "app-progress-circle",
  templateUrl: "./progress-circle.component.html",
  styleUrls: ["./progress-circle.component.scss"]
})
export class ProgressCircleComponent implements OnInit, OnChanges {
  @Input() radius = 50;
  @Input() value = 0;
  @Input() stroke = 5;
  @Input() color = "#00a6ff";
  @Input() animation = false;

  CIRCUMFERENCE: number;

  // diameter
  d: number;
  dashoffset: number;
  text: string;

  interval: any;

  constructor() { }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges) {
    this.CIRCUMFERENCE = 2 * Math.PI * this.radius;
    this.dashoffset = this.CIRCUMFERENCE;
    // console.log('change values');
    this.drawer();
  }

  drawer() {
    let percent = this.value;
    let deg = this.progress(percent);
    this.d = this.radius * 2 + 20;


    if (this.animation) {// Con animación
      let i = this.CIRCUMFERENCE, p = 0;

      if (this.interval) clearInterval(this.interval);
      this.interval = setInterval(() => {
        this.dashoffset = i;
        p = Math.round(100 - (i / this.CIRCUMFERENCE) * 100);

        this.text = `${p} %`;

        if (i <= deg) clearInterval(this.interval);
        i--;
      }, 1);
    } else {// Sin animación
      this.dashoffset = deg;
      this.text = `${this.value} %`;
    }
  }

  progress(value): number {
    let progress = value / 100;
    return this.CIRCUMFERENCE * (1 - progress);
  }
}
