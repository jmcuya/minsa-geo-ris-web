import { AfterViewInit, Component, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { fromEvent, Subscription } from 'rxjs';
import { filter, tap } from 'rxjs/operators';

@Component({
  selector: 'app-selector-lazy',
  templateUrl: './selector-lazy.component.html',
  styleUrls: ['./selector-lazy.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectorLazyComponent),
      multi: true
    }
  ]
})
export class SelectorLazyComponent implements OnInit, AfterViewInit, ControlValueAccessor {

  control: FormControl = new FormControl();
  show: boolean;
  isLoading: boolean;

  @Input() options: any;
  @Input() items: any[] = [];
  @Input() bindLabel: string;
  @Input() bindValue: string;

  @Output() selected = new EventEmitter();

  filtered: any[] = [];

  @ViewChild('inputSearch', { static: false }) inputSearch: ElementRef;
  @ViewChild('perfectScroll', { static: false }) perfectScroll: ElementRef;

  _value: any;
  onChange: (value: string) => void;
  onTouch: () => void;
  disabled: boolean;

  constructor() { }

  updateValue(): void {
    // Para que actualice el formControl debe llamarse a onChange
    if (this.onChange) {
      this.onChange(this._value);
    }
  }

  writeValue(value: any): void {
    this._value = value;
    this.control.setValue(value?.label);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
    if (isDisabled) this.control.disable();
    else this.control.enable();
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.watchControlSearch();
  }

  watchControlSearch(): void {
    let subscription: Subscription;

    // fromEvent(this.inputSearch.nativeElement, 'keyup')
    this.control.valueChanges
      .pipe(
        tap(value => {
          if (!value) {
            this._value = null;
            this.filtered = [];
            this.updateValue();
          }
        }),
        filter(value => !!value),
        tap(() => this.isLoading = true)
      )
      .subscribe(value => {
        if (this.options?.fn) {
          if (!this.show) {
            this.isLoading = false;
            return;
          };

          if (subscription) {
            subscription.unsubscribe();
          }

          subscription = this.options.fn(value)
            .subscribe((res: any[]) => {
              this.filtered = this.mapping(res.slice(0, 100), value);
              this.isLoading = false;
              if (this.perfectScroll) this.perfectScroll.nativeElement.scrollTop = 0;
            });
        } else {
          this.isLoading = false;
          this.filtered = this.items.filter(x => `${x[this.bindLabel]}`.toUpperCase().indexOf(value?.toUpperCase()) != -1);
          this.filtered = this.mapping(this.filtered, value);
          if (this.perfectScroll) this.perfectScroll.nativeElement.scrollTop = 0;
        }

      });
  }

  trackByItems(item: any): any {
    return item[this.bindValue];
  }

  mapping(values: any[], text: string) {
    return values.map(x => {
     // x._label = x[this.bindLabel]?.replace(new RegExp(`(${text})`, 'ig'), `<strong>$1</strong>`);

      return x;
    });
  }

  selectedOption(item: any): void {
    this.show = false;
    this._value = { value: item[this.bindValue], label: item[this.bindLabel] };
    this.control.setValue(item[this.bindLabel]);
    this.updateValue();
    this.filtered = this.mapping(this.filtered, this.control.value);
    this.selected.emit(item);
  }

}
