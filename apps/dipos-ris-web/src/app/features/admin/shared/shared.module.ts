import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MarketDirectivesModule, MarketHttpModule, MarketInterceptorModule, MarketPipesModule, MarketServicesModule } from "@dipos-ris-web-commons";
import { PerfectScrollbarConfigInterface, PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG } from "ngx-perfect-scrollbar";
import { SharedInterceptorModule } from "./interceptors/interceptors.module";

// ngx-bootstrap
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import '../shared/config/bsDatepicker';

import { BsModalService } from "./services/bs-modal.service";
import { ConfirmService } from "./services/confirm.service";
import { SelectorLazyComponent } from "./components/selector-lazy/selector-lazy.component";
import { LoginComponent } from "./components/login/login.component";
import { ProgressCircleComponent } from "./components/progress-circle/progress-circle.component";
import { ConfirmComponent } from "./components/confirm/confirm.component";
import { PaginatorComponent } from "./components/paginator/paginator.component";
import { PaginatorPipe } from "./components/paginator/paginator.pipe";

const COMPONENTS = [
  LoginComponent,
  ProgressCircleComponent,
  SelectorLazyComponent,  
  ConfirmComponent,
  PaginatorComponent
];

const PIPES = [
  PaginatorPipe
];

const MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  MarketDirectivesModule,
  MarketHttpModule,
  MarketServicesModule,
  MarketPipesModule,
  MarketInterceptorModule, // Adjuntar Authorization a cada request
  SharedInterceptorModule, // Mostrar el login cuando vence sesion
  PerfectScrollbarModule,    
];

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelPropagation: false
};

@NgModule({
  imports: [
    ...MODULES,
    BsDatepickerModule.forRoot()
  ],
  exports: [
    ...MODULES,
    BsDatepickerModule,
    ...COMPONENTS,
    ...PIPES
  ],
  declarations: [
    ...COMPONENTS,
    ...PIPES
  ],
  providers: [
    BsModalService, // Servicio personalizado sin librerias
    ConfirmService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    BsLocaleService
  ]
})
export class AdminSharedModule { 

  constructor(private bsLocaleService: BsLocaleService) { 
    this.bsLocaleService.use('es');
  }
}
