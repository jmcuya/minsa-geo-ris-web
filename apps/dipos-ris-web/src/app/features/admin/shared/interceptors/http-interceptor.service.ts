import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import {  Observable, of, throwError } from 'rxjs';
import { SessionService } from '@dipos-ris-web-commons';
import { catchError } from 'rxjs/operators';
import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { LoginComponent } from '@admin-shared/components/login/login.component';

@Injectable()
export class HttpErrorInterceptorService implements HttpInterceptor {

  show: boolean;

  constructor(
    // private router: Router,
    // private session: SessionService,
    private bsModal: BsModalService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let request = req.clone();
    return next.handle(request).pipe(
      catchError(err => this.handleAuthError(request, next, err))
    );
  }

  private handleAuthError(req: HttpRequest<any>, next: HttpHandler, error: HttpErrorResponse): Observable<any> {
    if (!this.show && error instanceof HttpErrorResponse && error.status === 401) {
      // if (req.headers.has('mylogin')) {      

      this.show = true;
      this.bsModal.open(LoginComponent).then(res => {        
        this.show = false;
      });
    }

    return throwError(error);

  }

}
