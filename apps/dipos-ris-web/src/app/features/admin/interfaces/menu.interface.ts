export interface ISidebarMenu {
  title: string;
  icon?: string;
  url?: string;
  params?: any;
  selected?: boolean;
  expand?: boolean;
  role?: string;
  childrens?: ISidebarMenu[]
}
