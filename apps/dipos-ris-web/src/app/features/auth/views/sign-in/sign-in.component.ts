import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService, SessionService } from '@dipos-ris-web-commons';
import { TokenDecoded, UserRol } from '@dipos-ris-web/models';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  form: FormGroup;
  isLoading: boolean;
  error_message: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activateRoute: ActivatedRoute,
    private session: SessionService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
    });
  }

  sigIn() {
    if (this.form.valid) {
      this.isLoading = true;
      this.authService.signIn(this.form.value).subscribe((token: TokenDecoded) => {
        this.session.create(token);
        const returnUrl = this.activateRoute.snapshot.queryParams.returnUrl;
        this.isLoading = false;
        this.router.navigateByUrl(returnUrl || '/');
      }, err => {
        this.isLoading = false;
        this.error_message = err?.error.error_description || err.message;
      });
    } else {
      this.form.markAllAsTouched();
      this.form.updateValueAndValidity();
    }
  }

}
