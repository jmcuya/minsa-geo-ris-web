import { Component, OnInit } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-website',
  templateUrl: './website.component.html',
  styleUrls: ['./website.component.scss']
})
export class WebsiteComponent implements OnInit {

  show: boolean = false;
  offline: boolean;

  constructor() { }

  ngOnInit() {
    const sidebar = localStorage.getItem('sidebar');
    this.show = !sidebar || sidebar === 'open';

    this.verifySignal();
  }

  verifySignal(): void {
    fromEvent(window, 'online')
      .pipe(
        debounceTime(250)
      ).subscribe(() => this.offline = false);

    fromEvent(window, 'offline')
      .pipe(
        debounceTime(250)
      ).subscribe(() => this.offline = true);
  }

}