import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WebsiteComponent } from './website.component';
import { InicioComponent } from './views/inicio/inicio.component';
import { MapaRisComponent } from './views/mapa-ris/mapa-ris.component';
import { DashboardRisComponent } from './views/dashboard-ris/dashboard-ris.component';
import {IntercambioPrestacionalComponent} from "./views/intercambio-prestacional/intercambio-prestacional.component";
import { ResolucionesConfirmacionRisComponent } from './views/resoluciones-confirmacion-ris/resoluciones-confirmacion-ris.component';
import {EquiposImpulsoresDeRisComponent} from "./views/equipos-impulsores-de-ris/equipos-impulsores-de-ris.component";

const routes: Routes = [
  {
    path: '',
    component: WebsiteComponent,
    children: [
      { path: '', pathMatch: 'full', component: MapaRisComponent },
			{
				path: 'inicio',
				component: InicioComponent
			},
      {
        path: '',
        component: MapaRisComponent
      },
      {
        path: 'dashboard',
        component: DashboardRisComponent
      },
      {
        path: 'intercambio-prestacional',
        component: IntercambioPrestacionalComponent
      },
      {
        path: 'resolucion-de-confirmacion-de-ris',
        component: ResolucionesConfirmacionRisComponent
      },
      {
        path: 'equipos-impulsores-de-ris',
        component: EquiposImpulsoresDeRisComponent
      },
      {
				path: 'etapas',
				loadChildren: () => import('./views/etapas/etapas.module').then(m => m.EtapasModule)
			},
      {
				path: 'usuarios',
				loadChildren: () => import('./views/usuarios/usuarios.module').then(m => m.UsuariosModule)
			},
      {
				path: 'PNIRIS',
				loadChildren: () => import('./views/PNIRIS/pniris.module').then(m => m.PNIRISModule)
			},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebsiteRoutingModule { }
