import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisorMetasFisicasComponent } from './visor-metas-fisicas.component';

describe('VisorMetasFisicasComponent', () => {
  let component: VisorMetasFisicasComponent;
  let fixture: ComponentFixture<VisorMetasFisicasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisorMetasFisicasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisorMetasFisicasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
