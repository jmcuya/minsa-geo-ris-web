import { Component, OnDestroy, OnInit } from '@angular/core';
import { FileUploadControl, FileUploadValidators } from '@iplab/ngx-file-upload';
import { Subscription } from 'rxjs';
import * as XLSX from 'xlsx';
type AOA = any[][];
@Component({
  selector: 'app-imp-matriz-seguimiento',
  templateUrl: './imp-matriz-seguimiento.component.html',
  styleUrls: ['./imp-matriz-seguimiento.component.scss']
})
export class ImpMatrizSeguimientoComponent implements OnInit ,OnDestroy {

  multiple: boolean = false;
  animation: boolean = true;
  editar:boolean = false;

  uploadedFiles: Array<File> = [];



  data: AOA = [];
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';

  private subscription: Subscription;

  public readonly control = new FileUploadControl(
    { listVisible: true, accept: ['.xlsx ', '.xls'], discardInvalid: true, multiple: false },
    [FileUploadValidators.accept(['.xlsx' , '.xls']), FileUploadValidators.filesLimit(1)]
);


  constructor() { }

  ngOnInit(): void {
    this.subscription = this.control.valueChanges.subscribe((values: Array<File>) => this.cargaData(values[0]));
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
}


  options(option , item = null) {

    switch(option){
      case 1 : console.log("delete"); break;
      case 2 : this.editar= true; break;
      case 3 : this.removeItem(item); break;
    }  
  }

  removeItem(indice){
    this.data.splice(indice,1);
  }

  cargaData(data){

    if(data !== undefined){
      this.onFileChange(data)
    }

    console.log("data", data)
  }


  onFileChange(datos) {
    /* wire up file reader */
   // const target: DataTransfer = <DataTransfer>(evt.target);
    //if (datos.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
      this.data = this.data.slice(3)
      console.log(this.data);
    };
    reader.readAsBinaryString(datos);
  }

}
