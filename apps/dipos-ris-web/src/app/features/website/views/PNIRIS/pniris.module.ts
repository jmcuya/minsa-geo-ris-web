import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PNIRISRoutingModule } from './pniris-routing.module';
import { ProMetasFisicasComponent } from './tabs/pro-metas-fisicas/pro-metas-fisicas.component';
import { ProPresupuestalComponent } from './tabs/pro-presupuestal/pro-presupuestal.component';
import { PNIRISComponent } from './pniris.component';
import { AuthChildsGuard } from '../../../admin/guards/auth-childs.guard';
import { NgApexchartsModule } from 'ng-apexcharts';
import { AdminSharedModule } from '@admin-shared/shared.module';
import { TabsModule} from 'ngx-bootstrap/tabs';
import { ImpMatrizSeguimientoComponent } from './imp-matriz-seguimiento/imp-matriz-seguimiento.component';
import { VisorMetasFisicasComponent } from './visor-metas-fisicas/visor-metas-fisicas.component';
import { VisorProgramacionPresupuestalComponent } from './visor-programacion-presupuestal/visor-programacion-presupuestal.component';
import { RegistrarMetasFisicasComponent } from './registrar-metas-fisicas/registrar-metas-fisicas.component';
import { RegistrarProgramacionPresupuestalComponent } from './registrar-programacion-presupuestal/registrar-programacion-presupuestal.component';
import { SupervisarMetasFisicasComponent } from './supervisar-metas-fisicas/supervisar-metas-fisicas.component';
import { SupervisarProgramacionPresupuestalComponent } from './supervisar-programacion-presupuestal/supervisar-programacion-presupuestal.component';

//import { NgxChartModule } from 'ngx-chart';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { FileUploadModule } from '@iplab/ngx-file-upload';
@NgModule({
  declarations: [
    PNIRISComponent,
    ProMetasFisicasComponent,
    ProPresupuestalComponent,
    ImpMatrizSeguimientoComponent,
    VisorMetasFisicasComponent,
    VisorProgramacionPresupuestalComponent,
    RegistrarMetasFisicasComponent,
    RegistrarProgramacionPresupuestalComponent,
    SupervisarMetasFisicasComponent,
    SupervisarProgramacionPresupuestalComponent
  ],
  imports: [
    CommonModule,
    PNIRISRoutingModule,
    AdminSharedModule,
    NgApexchartsModule,
    TabsModule.forRoot(),
    FileUploadModule,
    //NgxChartModule,
    NgxChartsModule,
    //NgxChartsModule,
   // BrowserAnimationsModule,
    //BrowserModule

  ],  
  providers: [
    AuthChildsGuard
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PNIRISModule { }
