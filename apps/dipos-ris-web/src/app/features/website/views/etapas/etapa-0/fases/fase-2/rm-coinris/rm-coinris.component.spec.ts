import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RMCOINRISComponent } from './rm-coinris.component';

describe('RMCOINRISComponent', () => {
  let component: RMCOINRISComponent;
  let fixture: ComponentFixture<RMCOINRISComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RMCOINRISComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RMCOINRISComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
