import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Ley30885Component } from './ley30885.component';

describe('Ley30885Component', () => {
  let component: Ley30885Component;
  let fixture: ComponentFixture<Ley30885Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Ley30885Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Ley30885Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
