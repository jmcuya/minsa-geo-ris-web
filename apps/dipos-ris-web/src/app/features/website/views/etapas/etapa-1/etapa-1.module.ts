import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Etapa1RoutingModule } from './etapa-1-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    Etapa1RoutingModule
  ]
})
export class Etapa1Module { }
