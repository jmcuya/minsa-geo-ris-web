import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaseUnoComponent } from './fase-uno.component';

describe('FaseUnoComponent', () => {
  let component: FaseUnoComponent;
  let fixture: ComponentFixture<FaseUnoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FaseUnoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FaseUnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
