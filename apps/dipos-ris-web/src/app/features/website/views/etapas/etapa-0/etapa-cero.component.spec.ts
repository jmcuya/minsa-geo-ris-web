import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtapaCeroComponent } from './etapa-cero.component';

describe('EtapaCeroComponent', () => {
  let component: EtapaCeroComponent;
  let fixture: ComponentFixture<EtapaCeroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtapaCeroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtapaCeroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
