import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PolGeneralGobiernoComponent } from './pol-general-gobierno.component';

describe('PolGeneralGobiernoComponent', () => {
  let component: PolGeneralGobiernoComponent;
  let fixture: ComponentFixture<PolGeneralGobiernoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PolGeneralGobiernoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PolGeneralGobiernoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
