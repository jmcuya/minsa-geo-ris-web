import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Fase2RoutingModule } from './fase-2-routing.module';
import { FaseDosComponent } from './fase-dos.component';
import { DocNormEspRegModRISComponent } from './doc-norm-esp-reg-mod-ris/doc-norm-esp-reg-mod-ris.component';
import { DocNormRelDimPresModRISComponent } from './doc-norm-rel-dim-pres-mod-ris/doc-norm-rel-dim-pres-mod-ris.component';
import { DocNormRelDimGestModRISComponent } from './doc-norm-rel-dim-gest-mod-ris/doc-norm-rel-dim-gest-mod-ris.component';
import { DocNormRelDimFinModRISComponent } from './doc-norm-rel-dim-fin-mod-ris/doc-norm-rel-dim-fin-mod-ris.component';
import { DocNormRelDimGobModRISComponent } from './doc-norm-rel-dim-gob-mod-ris/doc-norm-rel-dim-gob-mod-ris.component';
import { AboComImpNacRISComponent } from './abo-com-imp-nac-ris/abo-com-imp-nac-ris.component';
import { RMCOINRISComponent } from './rm-coinris/rm-coinris.component';
import { PlanTrabajoCOINRISComponent } from './plan-trabajo-coinris/plan-trabajo-coinris.component';
import { ActasCOINRISComponent } from './actas-coinris/actas-coinris.component';
import { InformesCOINRISComponent } from './informes-coinris/informes-coinris.component';


@NgModule({
  declarations: [
    FaseDosComponent,
    DocNormEspRegModRISComponent,
    DocNormRelDimPresModRISComponent,
    DocNormRelDimGestModRISComponent,
    DocNormRelDimFinModRISComponent,
    DocNormRelDimGobModRISComponent,
    AboComImpNacRISComponent,
    RMCOINRISComponent,
    PlanTrabajoCOINRISComponent,
    ActasCOINRISComponent,
    InformesCOINRISComponent
  ],
  imports: [
    CommonModule,
    Fase2RoutingModule
  ]
})
export class Fase2Module { }
