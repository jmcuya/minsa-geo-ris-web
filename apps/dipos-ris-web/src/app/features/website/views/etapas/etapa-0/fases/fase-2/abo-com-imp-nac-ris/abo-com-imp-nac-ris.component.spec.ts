import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboComImpNacRISComponent } from './abo-com-imp-nac-ris.component';

describe('AboComImpNacRISComponent', () => {
  let component: AboComImpNacRISComponent;
  let fixture: ComponentFixture<AboComImpNacRISComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboComImpNacRISComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AboComImpNacRISComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
