import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActasCOINRISComponent } from './actas-coinris.component';

describe('ActasCOINRISComponent', () => {
  let component: ActasCOINRISComponent;
  let fixture: ComponentFixture<ActasCOINRISComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActasCOINRISComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActasCOINRISComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
