import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FasesRoutingModule } from './fases-routing.module';
import { FasesComponent } from './fases.component';


@NgModule({
  declarations: [
    FasesComponent
  ],
  imports: [
    CommonModule,
    FasesRoutingModule
  ]
})
export class FasesModule { }
