import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Etapa3RoutingModule } from './etapa-3-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    Etapa3RoutingModule
  ]
})
export class Etapa3Module { }
