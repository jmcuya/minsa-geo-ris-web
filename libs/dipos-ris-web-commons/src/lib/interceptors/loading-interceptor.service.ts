import { Injectable, Inject } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { catchError, tap, finalize } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';
import { of, throwError } from 'rxjs';

@Injectable()
export class LoadingInterceptorService implements HttpInterceptor {
  private totalRequests = 0;

  constructor(
    @Inject(DOCUMENT) private document: Document
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    this.totalRequests++;
    let context: any;
    const ctx = request.headers.get('context');
    if (ctx) {
      context = this.document.querySelector(ctx);
      if (context) {
        const message = request.headers.get('caption') || 'Procesando...';
        context.setAttribute('loading', message);
        context.classList.add('loading');
      }
    }

    return next.handle(request).pipe(
      finalize(() => this.decreaseRequests(context))
    );
  }

  private decreaseRequests(context: any) {
    this.totalRequests = Math.max(this.totalRequests, 1) - 1;

    if (this.totalRequests === 0) {
      // all the http services
    }

    context?.classList.remove('loading');
  }

}
