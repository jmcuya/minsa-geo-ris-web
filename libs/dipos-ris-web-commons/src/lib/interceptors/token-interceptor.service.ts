import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { SessionService } from '../services/session.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { catchError, filter, switchMap, take } from 'rxjs/operators';
import { AuthService } from '../http/auth.service';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
	private isRefreshing = false;
	private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

	constructor(
		private session: SessionService,
		private authService: AuthService
	) { }

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		let request = req;

		// Verificamos el token del usuario que ha iniciado sesion
		const user = this.session.user;
		if (user) {
			request = this.addToken(req, user.token.access_token);
		}

		return next.handle(request)
			.pipe(
				catchError(error => {
					// Si se venció la sesion, pedimos un nuevo token
					if (error instanceof HttpErrorResponse && error.status === 401) {
						return this.handleAuthError(request, next, error);
					} else {
						return throwError(error);
					}
				})
			);
	}

	private addToken(request: HttpRequest<any>, token: string) {
		return request.clone({
			headers: request.headers.set('Authorization', `Bearer ${token}`)
		});
	}

	private handleAuthError(request: HttpRequest<any>, next: HttpHandler, error: any): Observable<any> {
		// console.log('Refresh token');
		// debugger
		if (!this.isRefreshing) {
			this.isRefreshing = true;
			this.refreshTokenSubject.next(null);

			const refresh_token = this.session.tokenStored?.refresh_token;
			if (!refresh_token) {
				return throwError(error);
			}

			return this.authService.refreshToken(refresh_token)
				.pipe(
					catchError(error => {
						// Si hay un error, quiere decir que venció el refresh_token (larga duración)
						// por tanto debemos mostrar/dirigir al login
						if (error instanceof HttpErrorResponse && error.status === 400) {
							// console.log('login...');							

							// Un artificio para hacer de nuevo la llamada y pasar por el interceptor que muestra el login
							let req = request.clone({
								headers: request.headers.set('mylogin', 'true')
							});

							return next.handle(req);
						}

						return throwError(error);
					}),
					switchMap((token: any) => {
						// console.log(token);                    
						this.isRefreshing = false;
						this.refreshTokenSubject.next(token);

						// Asignamos el nuevo token
						this.session.create(token);
						return next.handle(this.addToken(request, token.access_token));
					}),
				);
		} else {
			return this.refreshTokenSubject
				.pipe(
					take(1),
					switchMap(token => {
						if (token) {
							this.session.create(token);
							return next.handle(this.addToken(request, token.access_token));
						} else {
							return throwError(error);
						}
					})
				);
		}

		// return next.handle(request);
	}
}
