import { NgModule } from "@angular/core";
import { AuthenticatedGuard } from './authenticated.guard';
import { RolGuard } from './rol.guard';

@NgModule({
  imports: [],
  providers: [
    AuthenticatedGuard,
    RolGuard
  ]
})
export class MarketGuardsModule { }