import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "apps/dipos-ris-web/src/environments/environment";
import { Observable } from "rxjs";

@Injectable()
export class UserService {

  private apiBase = `${environment.apiURL}/api`;

  constructor(private http: HttpClient) { }

  roles(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiBase}/users/roles`);
  }

  user(userId: string): Observable<any> {
    const params = new HttpParams()
      .set('user_id', userId);

    return this.http.get<any>(`${this.apiBase}/users?${params.toString()}`);
  }

  create(user: any, isCreate: boolean): Observable<any> {
    const params = new HttpParams()
      .set('flagCreate', isCreate);

    return this.http.post<any>(`${this.apiBase}/users/create?${params.toString()}`, user);
  }

  remove(userId: string): Observable<boolean> {
    const params = new HttpParams()
      .set('user_id', userId);

    return this.http.delete<any>(`${this.apiBase}/users/remove?${params.toString()}`);
  }

}