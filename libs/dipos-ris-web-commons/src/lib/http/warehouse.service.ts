import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "apps/dipos-ris-web/src/environments/environment";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable()
export class WarehouseService {

  private apiBase = `${environment.apiURL}`;

  constructor(private http: HttpClient) { }

  getAll(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiBase}/api/warehouses`);
  }

  stock(request: any): Observable<any[]> {
    const params = new HttpParams()
      .set('id_warehouse', request.id_warehouse || null)
      .set('id_category', request.id_category || null)
      .set('id_trade', request.id_trade || null)
      .set('keywords', request.keywords || '');

    return this.http.get<any[]>(`${this.apiBase}/api/warehouses/search?${params.toString()}`)
      .pipe(
        map(res => res.map((p: any) => {
          p.image = `${this.apiBase}/content/products/${p.IdProducto}.png?v=${new Date().getTime()}`;
          return p;
        }))
      );
  }

  presentations(id_warehouse: number, id_product: number): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiBase}/api/warehouses/${id_warehouse}/product-presentations/${id_product}`);
  }

  transfer(source: any, target: any): Observable<any> {
    return this.http.post<any>(`${this.apiBase}/api/warehouses/transfer`, { source, target });
  }

  movementTypes(): Observable<any> {
    return this.http.get<any>(`${this.apiBase}/api/warehouses/movement-types`);
  }

  movementInsert(movements: any[]): Observable<any> {
    return this.http.post<any>(`${this.apiBase}/api/warehouses/movements-insert`, movements);
  }

}
